﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace TavroxLib
{
    [CustomEditor(typeof(CanvasGroup))]
    public class TextEditor : Editor
    {

        private void OnEnable()
        {

        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            EditorGUILayout.LabelField("Tavrox Lib");
            Text targ = (Text)target;
            if (GUILayout.Button("Add Translated text"))
            {
                targ.gameObject.AddComponent<TranslatedText>();
            }
        }
    }
}
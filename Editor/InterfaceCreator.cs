﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace TavroxLib
{
    public class InterfaceCreator
    {

        [MenuItem("GameObject/TavroxLib/ToggleGroup")]
        private static void createToggleGroup()
        {
            Debug.Log("Toggle Group");
            GameObject go = new GameObject("ToggleGroup");
            go.AddComponent<ToggleGroup>();

            GameObject ch = new GameObject("ToggleA");
            ch.transform.SetParent(go.transform);

            // TODO - Continue and add 2 buttons from here
        }
    }
}
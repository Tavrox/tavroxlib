﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

namespace TavroxLib
{
    [CustomEditor(typeof(TranslatedText))]
    [CanEditMultipleObjects]
    public class TranslatedTextEditor : Editor
    {

        public TextMeshProUGUI Text;
        public Text Alt;
        TranslatedText targ;

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            EditorGUILayout.LabelField("Tavrox Lib");
            targ = (TranslatedText)target;

            Object[] monoObjects = targets;

            if (GUILayout.Button("Set current text to text FR"))
            {
                getFr();
                if (targets.Length > 1)
                {
                    for (int i = 0; i < targets.Length; i++)
                    {
                        getFr();
                    }
                }

            }
            EditorUtility.SetDirty(targ);
            EditorSceneManager.MarkSceneDirty(targ.gameObject.scene);
        }

        void getFr()
        {
            Text = targ.GetComponent<TextMeshProUGUI>();
            Alt = targ.GetComponent<Text>();
            if (Text != null)
            {
                targ.textFR = Text.text;
            }
            if (Alt != null)
            {
                targ.textFR = Alt.text;
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TavroxLib
{
    public class TranslationController : MonoBehaviour
    {
        List<TranslatedText> Trans;

        void Awake()
        {
            Trans = GetComponentsInChildren<TranslatedText>().ToList();
            foreach (TranslatedText _t in Trans)
            {
                _t.Setup(this);
            }
        }

        public void TranslateAllTo(Languages _lang)
        {
            foreach (TranslatedText _t in Trans)
            {
                _t.Translate(_lang);
            }
        }
    }
}
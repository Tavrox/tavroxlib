﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TavroxLib
{
    public enum Languages
    {
        French,
        Spanish,
        English
    };

    public class TranslationData : ScriptableObject
    {
        List<string> Languages;
        
    }

}
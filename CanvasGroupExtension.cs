﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TavroxLib
{
    public static class CanvasGroupExtension
    {

        public static void Hide(this CanvasGroup _canvas)
        {
            _canvas.alpha = 0f;
            _canvas.blocksRaycasts = false;
            _canvas.interactable = false;
        }

        public static void Show(this CanvasGroup _canvas)
        {
            _canvas.alpha = 1f;
            _canvas.blocksRaycasts = true;
            _canvas.interactable = true;
        }


    }
}

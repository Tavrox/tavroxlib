﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TavroxLib
{

    public class TranslatedText : MonoBehaviour
    {
        // Must be put on top of Text or MeshProText

        public TextMeshProUGUI Text;
        public Text Alt;
        TranslationController Ctrl;
        [Multiline] public string textFR;
        [Multiline] public string textES;

        public TranslatedText Setup(TranslationController _ctrl)
        {
            Ctrl = _ctrl;
            Text = GetComponent<TextMeshProUGUI>();
            Alt = GetComponent<Text>();
            checkTrans();
            return this;
        }

        public void checkTrans()
        {
            if (string.IsNullOrEmpty(textFR))
            {
                Debug.LogError("Missing FR translation for" + name);
            }
            if (string.IsNullOrEmpty(textES))
            {
                Debug.LogError("Missing ES translation for" + name);
            }
        }

        public void Translate(Languages _lang)
        {
            switch (_lang)
            {
                case Languages.French:
                    if (Text != null)
                    {
                        Text.text = textFR;
                    }
                    if (Alt != null)
                    {
                        Alt.text = textFR;
                    }
                    break;
                case Languages.Spanish:
                    if (Text != null)
                    {
                        Text.text = textES;
                    }
                    if (Alt != null)
                    {
                        Alt.text = textES;
                    }
                    break;
            }
        }

    }
}